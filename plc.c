#define MAX_REGISTERS 1024

#include <string.h>
#include <ctype.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "plc.h"

typedef struct stack_t
{
	uint16_t data[16];
	uint16_t ptr;
} stack_t;

void plc_main(hal_vtable_t *hal)
{
	int i;
	uint16_t fault;

	memory_t mem;
	il_program_t program;
	stack_t prog_stack;
	uint16_t *regdata;
	uint16_t *stackdata;
	uint16_t *stackprevdata;
	uint16_t checksum;
	opcode_t opcode;

	/* Initialise memory to all zero. */
	memset(&mem.regs, 0, MAX_REGISTERS);

	/* Initialise the stack. */
	memset(&prog_stack.data, 0, 16);
	prog_stack.ptr = 0;

	/* Load the program into program memory. */
	hal->load_prog(&program);

	/* Calculate checksum and verify. */
	checksum = 0;
	for (i=0; i<MAX_INSTRUCTIONS; i++) {
		checksum += program.instructions[i].opcode;
		checksum += program.instructions[i].address & 0xFF;
		checksum += (program.instructions[i].address & 0xFF00) >> 8;
	}
	printf("Computed checksum is %d.\n", checksum);

	if (checksum == program.checksum) {
		/* Execute the program. */
		while (1) {
			/* Set the program loop timer. */
			hal->set_loop_timer();

			/* Initialise the stack. */
			prog_stack.ptr = 0;

			/* Read the inputs. */
			hal->read_inputs(&mem);

			/* Execute the instructions. */
			for (i=0; i<MAX_INSTRUCTIONS; i++) {
				opcode = program.instructions[i].opcode;
				regdata = &mem.regs[program.instructions[i].address];
				stackdata = &prog_stack.data[prog_stack.ptr];
				printf("Stack pointer: %d\n", prog_stack.ptr);
				printf("Stack data: %d\n", *stackdata);

				switch(opcode) {
					case NOP:
						// Do nothing.
						break;
					case AND:
						*stackdata = *stackdata && *regdata;
						printf("AND: %d\n", *stackdata);
						break;
					case OR:
						printf("stackdata: %d\n", *stackdata);
						printf("regdata: %d\n", *regdata);
						*stackdata = (*stackdata || *regdata);
						printf("OR: %d\n", *stackdata);
						break;
					case LD:
						prog_stack.ptr++;
						prog_stack.data[prog_stack.ptr] = *regdata;
						printf("LD: %d\n", *stackdata);
						break;
					case ST:
						*regdata = *stackdata;
						*stackdata = 0;
						prog_stack.ptr--;
						printf("ST: %d\n", *regdata);
						break;
					case ANB:
						if (prog_stack.ptr == 0) {
							fault |= (1<<STACK_DEPTH_FLT);
						} else {
							stackprevdata = &prog_stack.data[prog_stack.ptr - 1];
							*stackprevdata = *stackprevdata && *stackdata;
							*stackdata = 0;
							prog_stack.ptr--;
						}
						break;
					case ORB:
						if (prog_stack.ptr == 0) {
							fault |= (1<<STACK_DEPTH_FLT);
						} else {
							stackprevdata = &prog_stack.data[prog_stack.ptr - 1];
							*stackprevdata = *stackprevdata || *stackdata;
							*stackdata = 0;
							prog_stack.ptr--;
						}
						break;
					case END:
						i = MAX_INSTRUCTIONS;
						break;
					default:
						fault |= (1<<INSTR_VAL_FLT);
						i = MAX_INSTRUCTIONS;
				}
			}

			/* Write to outputs. */
			hal->write_outputs(&mem);

			/* Handle the modbus serial port. All memory accessible as holding regs. */

			/* Handle the programming serial port. CLI application with program facility. */

			/* Wait until program loop timer complete. */
			while (!hal->timer_done()) {}
		}
	}

	/* Infinite loop while faulted */
	hal->fault_handler();
	while(1);
}
