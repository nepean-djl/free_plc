/*
 * Author: Daniel Lloyd
 * Email: DanielLloyd7@gmail.com
 * 
 * PLC implementation using an Instruction List (IL) language stack based
 * virtual machine. The idea is to make a PLC that is as simple and cheap as
 * possible while still providing the main functionality - configuration without
 * downtime, debugging and monitoring. The only language that I would want to
 * implement is IL, which I have made a compiler for. Anybody should feel free
 * to extend it to other languages but it is not worth my time.
 *
 * It is a goal of the project to make it easily portable to hardware, so no
 * hardware implementation is provided. Just write your own HAL vtable and use
 * whatever hardware you want. It is my hope that this will allow fast
 * development of specialised controllers for machinery manufactured at high
 * volumes - even if these machines are only built by me :).
 */

#ifndef _PLC_H
#define _PLC_H

#define MAX_INSTRUCTIONS 1024
#define MAX_REGISTERS 1024

#define INSTR_VAL_FLT 0
#define STACK_DEPTH_FLT 1

#include <stdint.h>

typedef enum opcode_t {
	NOP,
	AND,
	OR,
	LD,
	ST,
	ANB,
	ORB,
	END
} opcode_t;


/* Instruction representation. */
typedef struct instruction_word_t
{
	uint8_t opcode;
	uint16_t address;
} instruction_word_t;

/* A program is just a list of instructions. */
typedef struct il_program_t
{
	instruction_word_t instructions[MAX_INSTRUCTIONS];
	uint16_t checksum;
} il_program_t;

typedef struct memory_t
{
	uint16_t regs[MAX_REGISTERS];
} memory_t;

/* HAL vtable that needs to be implemented to port your board. Example
 * implementation is given in the simulator application. */
typedef struct hal_vtable_t
{
	void (*read_inputs) (memory_t *mem);
	void (*write_outputs) (memory_t *mem);
	int (*read_modbus_port) (char *chr);
	int (*write_modbus_port) (char chr);
	int (*read_cli_port) (char *chr);
	int (*write_cli_port) (char chr);
	void (*set_loop_timer) ();
	int (*timer_done) ();
	void (*fault_handler) ();
	void (*load_prog) (il_program_t *program);
	void (*store_prog) (il_program_t *program);
} hal_vtable_t;

/* Pre-populate your HAL vtable with functions and call this routine to execute
 * the PLC. */
void plc_main(hal_vtable_t *hal);

#endif
