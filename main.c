#include "plc.h"

#include <stdio.h>

void read_inputs(memory_t *mem);
void write_outputs(memory_t *mem);
void set_loop_timer();
int timer_done();
void fault_handler();
void load_prog(il_program_t *program);

int main(void)
{
	hal_vtable_t hal;

	hal.read_inputs = read_inputs;
	hal.write_outputs = write_outputs;
	hal.set_loop_timer = set_loop_timer;
	hal.timer_done = timer_done;
	hal.fault_handler = fault_handler;
	hal.load_prog = load_prog;

	plc_main(&hal);
}

void read_inputs(memory_t *mem)
{
	printf("Enter data for input %I01 > ");
	fflush(stdout);
	scanf("%d", &mem->regs[0]);

	printf("Enter data for input %I02 > ");
	fflush(stdout);
	scanf("%d", &mem->regs[1]);

	printf("Enter data for input %I03 > ");
	fflush(stdout);
	scanf("%d", &mem->regs[2]);

	printf("Enter data for input %I04 > ");
	fflush(stdout);
	scanf("%d", &mem->regs[3]);

	printf("Enter data for input %I05 > ");
	fflush(stdout);
	scanf("%d", &mem->regs[4]);
}

void write_outputs(memory_t *mem)
{
	printf("Writing output %Q01 as %d.\n", mem->regs[256]);
	printf("Writing output %Q02 as %d.\n", mem->regs[257]);
	printf("Writing output %Q03 as %d.\n", mem->regs[258]);
	fflush(stdout);
}

void set_loop_timer() {
	printf("Setting loop timer.\n");
	fflush(stdout);
} 

int timer_done()
{
	printf("Loop timer complete.\n");
	fflush(stdout);
	return 1;
}

void fault_handler()
{
	printf("Setting all outputs to zero.");
	fflush(stdout);
}

void load_prog(il_program_t *program)
{
	printf("Loading program file...\n");
	fflush(stdout);
	FILE *fp = fopen("program.dat", "r");
	for (int i=0; i<MAX_INSTRUCTIONS; i++) {
		fread(&program->instructions[i].opcode, 1, 1, fp);
		fread(&program->instructions[i].address, 2, 1, fp);
	}
	fread(&program->checksum, 2, 1, fp);
	printf("File checksum is %d.\n", program->checksum);
	printf("Finished loading program.\n");
	fflush(stdout);
}
