All: main.exe il.exe program.dat

main.exe: main.c plc.o plc.h
	gcc main.c -o main.exe plc.o

plc.o: plc.c plc.h
	gcc -c plc.c

il.exe: IL_Compiler.c plc.h
	gcc IL_Compiler.c -o il.exe

program.dat: IL_Example.txt
	./il.exe < IL_Example.txt > program.dat
