/*
 * Compiler for an instruction list language. Converts the instruction list
 * into a binary sequence that is able to be interpreted by an instruction
 * list interpreter on a microcontroller.
 */

#include <string.h>
#include <ctype.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "plc.h"

#define MAX_LINE_SIZE 80
#define MAX_TOKENS 50

int main(void)
{
	il_program_t program;
	char buf[MAX_LINE_SIZE];
	char *token;
	char tokens[MAX_LINE_SIZE][MAX_TOKENS];
	char instruction[MAX_LINE_SIZE];
	char address[MAX_LINE_SIZE];
	char instruction_invalid, address_invalid, compile_fail;
	opcode_t opcode;
	uint16_t physical_address;
	uint16_t address_type_offset;
	uint16_t checksum;
	uint16_t error_count;
	int i, j, k;
	int linenum;

	memset(&program, 0, sizeof(il_program_t));

	i = 0;
	compile_fail = 0;
	error_count = 0;
	linenum = 0;

	/* Parse the file line by line. */
	while (gets(buf) != NULL) {
		linenum++;
		
		/* Remove comments and trailing spaces. */
		for  (j=0; j<strlen(buf); j++) {
			if (buf[j] == ';')
				buf[j] = 0;
		}

		/* Ignore blank lines. */
		if (!strcmp(buf, "")) {
			continue;
		}

		instruction_invalid = 0;
		address_invalid = 0;

		j = 0;
		token = strtok(buf, " ");
		while (token != NULL) {
			strncpy(tokens[j], token, MAX_LINE_SIZE);
			j++;
			token = strtok(NULL, buf);
		}

		/* Get instruction. */
		j = 0;
		while (!strcmp(tokens[j], "")) {
			j++;
		}
		strncpy(instruction, tokens[j], MAX_LINE_SIZE);

		/* Get address. */
		j++;
		strncpy(address, tokens[j], MAX_LINE_SIZE);

		/* Convert instruction string to opcode. */
		if (!strcmp(instruction, "AND")) {
			opcode = AND;
		} else if (!strcmp(instruction, "OR")) {
			opcode = OR;
		} else if (!strcmp(instruction, "LD")) {
			opcode = LD;
		} else if (!strcmp(instruction, "ST")) {
			opcode = ST;
		} else if (!strcmp(instruction, "ANB")) {
			opcode = ANB;
		} else if (!strcmp(instruction, "ORB")) {
			opcode = ORB;
		} else if (!strcmp(instruction, "END")) {
			opcode = END;
		} else {
			fprintf(stderr, "Line %d: Instruction \"%s\" not recognised.\n", linenum, instruction);
			instruction_invalid = 1;
			error_count++;
		}

		/*  Convert address string to physical address number. */
		physical_address = 0;
		if (opcode == ANB || opcode == ORB || opcode == END) {
			// No checking needed.
		} else if (!strcmp(address, "")) {
			fprintf(stderr, "Line %d: Invalid address \"%s\". Instruction \"%s\" needs an address.\n", linenum, address, instruction);
			address_invalid = 1;
			error_count++;
		} else if (address[0] != '%') {
			fprintf(stderr, "Line %d: Invalid address \"%s\". Address must start with a per-cent character.\n", linenum, address);
			address_invalid = 1;
			error_count++;
		} else if (strlen(address) > 7) {
			fprintf(stderr, "Line %d: Invalid address \"%s\". Address too long.\n", linenum, address);
			address_invalid = 1;
			error_count++;
		} else if (address[1] != 'I' && address[1] != 'Q' && address[1] != 'M') {
			fprintf(stderr, "Line %d: Invalid address \"%s\". Address type invalid.\n", linenum, address);
			address_invalid = 1;
			error_count++;
		} else {
			for (k=2; k<strlen(address)-1; k++) {
				if (!isdigit(address[k])) {
					fprintf(stderr, "Line %d: Invalid address %s. Address is a number.\n", linenum, address);
					address_invalid = 1;
					error_count++;
				}
			}

			if (!address_invalid) {
				address_type_offset = 0;

				switch (address[1]) {
					case 'I':
						address_type_offset = 0;
						break;
					case 'Q':
						address_type_offset = 256;
						break;
					case 'M':
						address_type_offset = 512;
						break;
					default:
						fprintf(stderr, "Error creating address number. Invalid address type \"%c\".\n", address[1]);
						error_count++;
				}

				physical_address = address_type_offset + atoi(address + 2) - 1;
			}
		}


		/* If line was valid, add instruction to the program. */
		if (!instruction_invalid && !address_invalid) {
			program.instructions[i].opcode = opcode;
			program.instructions[i].address = physical_address;
		} else {
			compile_fail = 1;
		}

		i++;
	}

	/* If compile failed, print to stderr and exit. */
	if (compile_fail) {
		fprintf(stderr, "Compile failed with %d errors.\n", error_count);
		return -1;
	/* If compile succeeded, then dump the program to stdout. */
	} else {
		checksum = 0;
		for (i=0; i<MAX_INSTRUCTIONS; i++) {
			fwrite(&program.instructions[i].opcode, 1, 1, stdout);
			fwrite(&program.instructions[i].address, 1, 2, stdout);
			checksum += program.instructions[i].opcode;
			checksum += program.instructions[i].address & 0xFF;
			checksum += (program.instructions[i].address & 0xFF00) >> 8;
		}
		fwrite(&checksum, 1, 2, stdout);
	}

	return 0;
}
